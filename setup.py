from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Repositório para ser utilizado no desafio de PAD',
    author='Evellyn Nicole',
    license='MIT',
)
